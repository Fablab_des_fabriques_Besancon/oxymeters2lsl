#!/usr/bin/env python

'''
Debian
https://pypi.org/project/pyliblo3/
https://github.com/gesellkammer/pyliblo3

$ sudo apt install liblo-dev
$ sudo apt install python3-liblo

'''


from __future__ import print_function
import liblo, sys
from pylsl import StreamInfo, StreamOutlet

# create server, listening on port 1234
try:
    server = liblo.Server(5000)
    # LSL
    osc_info = StreamInfo(name='osc_stream', type='Markers', channel_count=1, channel_format='string', source_id='osc_stream_001')
    osc_outlet = StreamOutlet(osc_info)

except liblo.ServerError as err:
    print(err)
    sys.exit()

def fallback(path, args, types, src):
    print("got a message '%s' from '%s'" % (path, src.url))
    for a, t in zip(args, types):
        print("argument of type '%s': %s" % (t, a))
        marker= path+" "+str(a)
        print(marker)
        osc_outlet.push_sample([marker])

# register a fallback for unhandled messages
server.add_method(None, None, fallback)

# loop and dispatch messages every 100ms
while True:
    server.recv(100)

