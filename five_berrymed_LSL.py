'''
Command to launch the project

$ python3 five_berrymed_LSL.py --by-address "00:a0:50:XX:XX:XX" "00:a0:50:XX:XX:XX" "00:a0:50:XX:XX:XX" "00:a0:50:XX:XX:XX" "00:a0:50:XX:XX:XX" 

'''

import argparse
import asyncio
import contextlib
import logging

from typing import Iterable

from bleak import BleakScanner, BleakClient
from pylsl import StreamInfo, StreamOutlet

uuid = "49535343-1e4d-4bd9-ba61-23c647249616"

async def connect_to_device(
    lock: asyncio.Lock,
    by_address: bool,
    macos_use_bdaddr: bool,
    name_or_address: str,
    notify_uuid: str,
):
    """
    Scan and connect to a device then print notifications for 10 seconds before
    disconnecting.

    Args:
        lock:
            The same lock must be passed to all calls to this function.
        by_address:
            If true, treat *name_or_address* as an address, otherwise treat
            it as a name.
        macos_use_bdaddr:
            If true, enable hack to allow use of Bluetooth address instead of
            UUID on macOS.
        name_or_address:
            The Bluetooth address/UUID of the device to connect to.
        notify_uuid:
            The UUID of a characteristic that supports notifications.
    """
    logging.info("starting %s task", name_or_address)

    try:
        async with contextlib.AsyncExitStack() as stack:

            # Trying to establish a connection to two devices at the same time
            # can cause errors, so use a lock to avoid this.
            async with lock:
                logging.info("scanning for %s", name_or_address)

                if by_address:
                    device = await BleakScanner.find_device_by_address(
                        name_or_address, macos=dict(use_bdaddr=macos_use_bdaddr)
                    )
                else:
                    device = await BleakScanner.find_device_by_name(name_or_address)

                logging.info("stopped scanning for %s", name_or_address)

                if device is None:
                    logging.error("%s not found", name_or_address)
                    return

                client = BleakClient(device)

                logging.info("connecting to %s", name_or_address)

                await stack.enter_async_context(client)

                logging.info("connected to %s", name_or_address)

                # This will be called immediately before client.__aexit__ when
                # the stack context manager exits.
                stack.callback(logging.info, "disconnecting from %s", name_or_address)

            # The lock is released here. The device is still connected and the
            # Bluetooth adapter is now free to scan and connect another device
            # without disconnecting this one.

            def callback(_, data):
                #logging.info("%s received %r", name_or_address, data)
                # BPM
                values = list(data)
                pleth1 = values[1]
                pleth2 = values[2]
                bpm = values[3]
                #print(type(bpm))
                print (name_or_address, pleth1, pleth2,bpm)
                
                if name_or_address == args.device1:
                    d1_outlet.push_sample([pleth1, pleth2, bpm])
                elif name_or_address == args.device2:
                    d2_outlet.push_sample([pleth1, pleth2, bpm])
                elif name_or_address == args.device3:
                    d3_outlet.push_sample([pleth1, pleth2, bpm])
                elif name_or_address == args.device4:
                    d4_outlet.push_sample([pleth1, pleth2, bpm])
                elif name_or_address == args.device5:
                    d5_outlet.push_sample([pleth1, pleth2, bpm])
                
            


            await client.start_notify(notify_uuid, callback)
            #await asyncio.sleep(0)
            await asyncio.Future()
            await client.stop_notify(notify_uuid)

        # The stack context manager exits here, triggering disconnection.

        logging.info("disconnected from %s", name_or_address)

    except Exception:
        logging.exception("error with %s", name_or_address)


async def main(
    by_address: bool,
    macos_use_bdaddr: bool,
    addresses: Iterable[str],
    uuids: Iterable[str],
):
    lock = asyncio.Lock()

    await asyncio.gather(
        *(
            connect_to_device(lock, by_address, macos_use_bdaddr, address, uuid)
            for address, uuid in zip(addresses, uuids)
        )
    )


if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument(
        "device1",
        metavar="<device>",
        help="Bluetooth name or address of first device connect to",
    )
    parser.add_argument(
        "device2",
        metavar="<device>",
        help="Bluetooth name or address of second device to connect to",
    )
    parser.add_argument(
        "device3",
        metavar="<device>",
        help="Bluetooth name or address of tird device to connect to",
    )
    parser.add_argument(
        "device4",
        metavar="<device>",
        help="Bluetooth name or address of fourth device to connect to",
    )
    parser.add_argument(
        "device5",
        metavar="<device>",
        help="Bluetooth name or address of fifth device to connect to",
    )
    parser.add_argument(
        "--by-address",
        action="store_true",
        help="when true treat <device> args as Bluetooth address instead of name",
    )

    parser.add_argument(
        "--macos-use-bdaddr",
        action="store_true",
        help="when true use Bluetooth address instead of UUID on macOS",
    )

    parser.add_argument(
        "-d",
        "--debug",
        action="store_true",
        help="sets the log level to debug",
    )

    args = parser.parse_args()

    log_level = logging.DEBUG if args.debug else logging.INFO
    logging.basicConfig(
        level=log_level,
        format="%(asctime)-15s %(name)-8s %(levelname)s: %(message)s",
    )

    # LSL
    d1_info = StreamInfo(name=args.device1, type='Markers', channel_count=3, channel_format='float32', source_id=args.device1)
    d1_outlet = StreamOutlet(d1_info)
    d2_info = StreamInfo(name=args.device2, type='Markers', channel_count=3, channel_format='float32', source_id=args.device2)
    d2_outlet = StreamOutlet(d2_info)
    d3_info = StreamInfo(name=args.device3, type='Markers', channel_count=3, channel_format='float32', source_id=args.device3)
    d3_outlet = StreamOutlet(d3_info)
    d4_info = StreamInfo(name=args.device4, type='Markers', channel_count=3, channel_format='float32', source_id=args.device4)
    d4_outlet = StreamOutlet(d4_info)
    d5_info = StreamInfo(name=args.device5, type='Markers', channel_count=3, channel_format='float32', source_id=args.device5)
    d5_outlet = StreamOutlet(d5_info)

    asyncio.run(
        main(
            args.by_address,
            args.macos_use_bdaddr,
            (args.device1, args.device2, args.device3, args.device4, args.device5),
            (uuid, uuid, uuid, uuid, uuid),
        )
    )
